const X = 'X';
let dico = [];
var allShow = [];
var refs = null;
var x, y;

//Gen 0/1/2/3/4 or X for dico
exports.Decouverte = (x, y) => {
    let count = 0;

    count = count + this.DecouverteZoneAll(x - 1, y);
    count = count + this.DecouverteZoneAll(x + 1, y);
    count = count + this.DecouverteZoneAll(x - 1, y + 1);
    count = count + this.DecouverteZoneAll(x - 1, y - 1);
    count = count + this.DecouverteZoneAll(x + 1, y + 1);
    count = count + this.DecouverteZoneAll(x + 1, y - 1);
    count = count + this.DecouverteZoneAll(x, y - 1);
    count = count + this.DecouverteZoneAll(x, y + 1);

    return count;
};

//Get mine for Decouverte
exports.DecouverteZoneAll = (x, y) => {
    if ((x <= MaxColl && x >= 0) && (y <= MaxLigne && y >= 0)) {
        if (this.GetMine(x, y) === true) {
            return 1;
        }
    }
    return 0
};

// Retourne la constente du Dico
exports.GetDico = () => {
    return dico;
};

//Get Mine in pos
exports.GetMine = (x, y) => {
    const info = dico[y][x];

    if (info === X) {
        return true;
    } else {
        return info;
    }
};

//Array include Object
exports.include = (arr, obj) => {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === obj) return true;
    }
    return false;
};

//CLick Event
exports.OnClickEvent = (senddico, ref, x, y, end = false) => {

    dico = senddico;
    refs = ref;
    let children = ref.$children[0].$children;
    let message = ref.$el.getElementsByClassName('message')[0];
    let element;

    //Retrieval of the element by the parent
    for (var i = 0; i < children.length; i++) {
        if ((children[i].$options.propsData['x'] === x) &&
            (children[i].$options.propsData['y'] === y)) {
            element = children[i].$el;
        }
    }

    //box already click
    if (this.include(allShow, x + '_' + y) === false) {
        allShow.push(x + '_' + y);
        const detected = this.GetMine(x, y);
        if (detected === true) {
            if (end === false) {
                element.src = '/static/img/mine_clique.jpg';
                this.DecouverteAllMine(senddico, x, y);
                message.innerHTML = 'Vous avez perdu !';
            } else {
                element.src = '/static/img/mine.jpg';
            }
        } else {
            if (detected === '0') {
                this.DecouverteZoneZero(senddico, x, y);
            }
            element.src = '/static/img/' + detected + '.jpg';
            message.innerHTML = 'Il y a ' + detected +
                ' mine autour de vous.';
        }
    }
};

//Finds all the 0 boxes in relation to each other
exports.DecouverteZoneZero = (senddico, x, y) => {
    this.DecouverteZoneZeroAll(senddico, x - 1, y);
    this.DecouverteZoneZeroAll(senddico, x + 1, y);
    this.DecouverteZoneZeroAll(senddico, x - 1, y + 1);
    this.DecouverteZoneZeroAll(senddico, x - 1, y - 1);
    this.DecouverteZoneZeroAll(senddico, x + 1, y + 1);
    this.DecouverteZoneZeroAll(senddico, x + 1, y - 1);
    this.DecouverteZoneZeroAll(senddico, x, y - 1);
    this.DecouverteZoneZeroAll(senddico, x, y + 1);

};

//Sends a click if the box is 0
exports.DecouverteZoneZeroAll = (senddico, x, y) => {
    if ((x <= MaxColl && x >= 0) && (y <= MaxLigne && y >= 0)) {
        var retourGet = this.GetMine(x, y);

        if (retourGet !== true) {
            this.OnClickEvent(senddico, refs, x, y);
        }
    }
};

//Discovers all the mines of the dictionary
exports.DecouverteAllMine = (senddico, x, y) => {
    var ligne = null;
    for (var yfor = 0; yfor < senddico.length; yfor++) {
        ligne = senddico[yfor];
        for (var xfor = 0; xfor < ligne.length; xfor++) {
            if (senddico[yfor][xfor] === X) {
                this.OnClickEvent(senddico, refs, xfor, yfor, true);
            } else {
                allShow.push(xfor + '_' + yfor);
            }

        }
    }
};

//Gen Dico
for (y = 0; y < 12; y++) {

    dico.push([]);

    for (x = 0; x < 17; x++) {

        if (Math.floor(Math.random() * Math.floor(100)) > 85) {
            dico[y].push('X');
        } else {
            dico[y].push('0');
        }
    }
}

const MaxLigne = dico.length - 1;
const MaxColl = dico[MaxLigne].length - 1;

for (y = 0; y < 12; y++) {
    for (x = 0; x < 17; x++) {
        if (dico[y][x] !== "X"){
            dico[y][x] = '' + this.Decouverte(x, y);
        }
    }
}
