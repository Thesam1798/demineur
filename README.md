## Démineur  
Structure des fichiers  

``` bash  
+---src                      Sources du project 
|   |   App.vue              Enter des routes
|   |   main.js              Enter de Vue.js
|   |                        
|   +---assets               
|   |       logo.png         Logo Vue.js
|   |                        
|   +---components           
|   |       case.vue         Composent d une case
|   |       demineur.vue     Toute les case
|   |                        
|   +---lib                  
|   |       LibDemineur.js   Libreri pour le démineur
|   |                        
|   +---router               
|   |       index.js         Router
|   |                        
|   \---vue                  
|           Start.vue        Entrer des view
|                            
\---static                   Object Static comme les images
    \---img...                  
```  

Lancer le démineur : ``` npm run dev  ```

Cela créer un server web local sur le port : 8080

## Build Setup  
  
``` bash  
# installer les dépendances  
npm install  
  
# Servir avec recharge à chaud sur localhost:8080  
npm run dev  
  
# construction pour la production avec minification  
npm run build  
  
# construire pour la production et visualiser le rapport de l'analyseur de bundle  
npm run build --report  
  
# effectuer des tests unitaires  
npm run unit  
  
# run e2e tests  
npm run e2e  
  
# run all tests  
npm test  
```  
  
Pour des explications détaillées sur le fonctionnement, consultez [http://vuejs.github.io/vue-loader](http://vuejs.github.io/vue-loader)
